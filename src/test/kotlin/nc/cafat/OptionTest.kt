package nc.cafat

import arrow.core.andThen
import io.kotest.assertions.withClue
import io.kotest.core.spec.style.WordSpec
import io.kotest.matchers.optional.shouldBeEmpty
import io.kotest.matchers.optional.shouldBePresent
import io.kotest.matchers.should
import io.kotest.matchers.shouldBe
import io.kotest.matchers.string.shouldNotBeBlank
import java.util.Optional
import kotlin.reflect.jvm.jvmName
import arrow.core.None as ANone
import arrow.core.Option as AOption
import arrow.core.Some as ASome
import io.vavr.control.Option as VOption

class OptionTest : WordSpec({

    "Optional" should {
        "have a type" {
            Optional::class.jvmName.shouldNotBeBlank()
        }

        "have a unit function" {
            Optional.ofNullable("").shouldBePresent()
            Optional.ofNullable(null).shouldBeEmpty()
        }

        "have a bind operation" {
            Optional.ofNullable("Hello world")
                .flatMap { Optional.ofNullable(it.length) } shouldBePresent { it shouldBe 11 }
        }

        "comply with left identity" {
            val f = { x: Int? ->
                Optional.ofNullable(when (x) {
                    null -> -1
                    2 -> null
                    else -> x + 1
                })
            }

            withClue("non empty result") {
                Optional.of(1).flatMap(f) shouldBe f(1)
            }

            withClue("empty result") {
                Optional.of(2).flatMap(f) shouldBe f(2)
            }

            withClue("empty inputs") {
                Optional.ofNullable(null as Int?).flatMap(f) shouldBe f(null)
            }
        }

        "comply with right identity" {
            withClue("empty input") {
                Optional.ofNullable(null as Int?).flatMap { Optional.ofNullable(it) }.shouldBeEmpty()
            }

            withClue("non empty input") {
                Optional.ofNullable(1).flatMap { Optional.ofNullable(it) } shouldBe Optional.ofNullable(1)
            }
        }

        "comply with associativity" {
            val f = { x: Int -> if (x % 2 == 0) null else x }
            val g = { y: Int? -> y ?: "no value" }

            val opt = Optional.of(2)

            opt.map(f).map(g) shouldBe opt.map(f andThen g)
        }
    }

    "Arrow Option" should {
        "have a type" {
            AOption::class.jvmName.shouldNotBeBlank()
        }

        "have a unit function" {
            AOption("") should {
                it.isDefined() shouldBe true
                it shouldBe ASome("")
            }

            AOption(null) should {
                it.isDefined() shouldBe true
                it shouldBe ASome(null)
            }

            ANone should {
                it.isDefined() shouldBe false
                it shouldBe ANone
            }
        }

        "have a bind operation" {
            AOption("Hello world")
                .flatMap { AOption(it.length) } shouldBe ASome(11)
        }

        "comply with left identity" {
            val f = { x: Int? ->
                AOption(when (x) {
                    null -> -1
                    2 -> null
                    else -> x + 1
                })
            }

            withClue("non empty result") {
                AOption(1).flatMap(f) shouldBe f(1)
            }

            withClue("empty result") {
                AOption(2).flatMap(f) shouldBe f(2)
            }

            withClue("empty inputs") {
                AOption(null).flatMap(f) shouldBe f(null)
            }
        }

        "comply with right identity" {
            withClue("empty input") {
                AOption(null as Int?).flatMap { AOption(it) } shouldBe AOption(null as Int?)
            }

            withClue("non empty input") {
                AOption(1).flatMap { AOption(it) } shouldBe AOption(1)
            }
        }

        "comply with associativity" {
            val f = { x: Int -> if (x % 2 == 0) null else x }
            val g = { y: Int? -> y ?: "no value" }

            val opt = AOption(2)

            opt.map(f).map(g) shouldBe opt.map(f andThen g)
        }
    }

    "Vavr Option" should {
        "have a type" {
            VOption::class.jvmName.shouldNotBeBlank()
        }
        
        "have a unit function" {
            VOption.some("") should {
                it.isDefined shouldBe true
                it shouldBe VOption.some("")
            }

            VOption.some(null) should {
                it.isDefined shouldBe true
                it shouldBe VOption.some(null)
            }

            VOption.none<String>() should {
                it.isDefined shouldBe false
                it shouldBe VOption.none()
            }
        }

        "have a bind operation" {
            VOption.some("Hello world")
                .flatMap { VOption.some(it.length) } shouldBe VOption.some(11)
        }

        "comply with left identity" {
            val f = { x: Int? ->
                VOption.some(when (x) {
                    null -> -1
                    2 -> null
                    else -> x + 1
                })
            }

            withClue("non empty result") {
                VOption.some(1).flatMap(f) shouldBe f(1)
            }

            withClue("empty result") {
                VOption.some(2).flatMap(f) shouldBe f(2)
            }

            withClue("empty inputs") {
                VOption.some(null).flatMap(f) shouldBe f(null)
            }
        }

        "comply with right identity" {
            withClue("empty input") {
                VOption.some(null as Int?).flatMap { VOption.some(it) } shouldBe VOption.some(null as Int?)
            }

            withClue("non empty input") {
                VOption.some(1).flatMap { VOption.some(it) } shouldBe VOption.some(1)
            }
        }

        "comply with associativity" {
            val f = { x: Int -> if (x % 2 == 0) null else x }
            val g = { y: Int? -> y ?: "no value" }

            val opt = VOption.some(2)

            opt.map(f).map(g) shouldBe opt.map(f andThen g)
        }
    }

})